import 'package:camera/camera.dart';
import 'package:envision_test/Utils/Theme.dart';
import 'package:envision_test/Utils/global.dart';
import 'package:flutter/material.dart';

import 'Fragments/capture.dart';
import 'Fragments/librery.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  CameraController _cameraController;

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (_cameraController != null) {
      await _cameraController.dispose();
    }
    _cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.low,
      enableAudio: true,
    );
    _cameraController.addListener(() {
      if (mounted) setState(() {});
      if (_cameraController.value.hasError) {
        print('CameraError ${_cameraController.value.errorDescription}');
      }
    });
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (_cameraController == null || !_cameraController.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      _cameraController?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (_cameraController != null) {
        onNewCameraSelected(_cameraController.description);
      }
    }
  }

  List<CameraDescription> cameras;
  int selectedCameraIdx;

  Future<void> _onCameraSwitched(CameraDescription cameraDescription) async {
    if (_cameraController != null) {
      await _cameraController.dispose();
    }

    _cameraController =
        CameraController(cameraDescription, ResolutionPreset.high);
    _cameraController.addListener(() {
      if (mounted) {
        setState(() {});
      }

      if (_cameraController.value.hasError) {
        print('CameraError' + '${_cameraController.value}');
      }
    });

    try {
      await _cameraController.initialize();
    } on CameraException catch (e) {
      print('CameraException' + '${e}');
    }

    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      if (cameras.length > 0) {
        setState(() {
          selectedCameraIdx = 0;
        });

        _onCameraSwitched(cameras[selectedCameraIdx]).then((void v) {});
      }
    }).catchError((error) {
      print('CameraError' + '${error.message} & ${error.code}');
    });

    tabController = new TabController(vsync: this, length: 2, initialIndex: 1);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Envision AI"),
          centerTitle: true,
          backgroundColor: AppColors.primary,
          bottom: TabBar(
            controller: tabController,
            labelStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
            tabs: [
              Tab(
                text: "CAPTURE",
              ),
              Tab(text: "LIBRARY"),
            ],
            //isScrollable: true,
            indicatorColor: Colors.white,
          ),
        ),
        body: TabBarView(
          controller: tabController,
          children: [
            Capture(_cameraController),
            Librery(),
          ],
        ),
      ),
    );
  }
}
