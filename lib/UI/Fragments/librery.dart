import 'package:envision_test/Models/cached_data.dart';
import 'package:envision_test/Models/content.dart';
import 'package:envision_test/UI/nodata.dart';
import 'package:envision_test/Utils/global.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../result.dart';

class Librery extends StatefulWidget {
  const Librery({Key key}) : super(key: key);

  @override
  _LibreryState createState() => _LibreryState();
}

class _LibreryState extends State<Librery> {
  SharedPreferences prefs;
  getCache() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.getString("content") == null ||
        prefs.getString("content").isEmpty) {
      setState(() {
        allData = [];
      });
    } else {
      setState(() {
        allData = CachedData.decode(prefs.getString("content"));
      });
    }
  }

  @override
  void initState() {
    getCache();

    super.initState();
  }

  Content content;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: allData.length == 0
          ? NoData(
              "No history yet!", "Please add some data from the CAPTURE tab")
          : ListView.builder(
              itemCount: allData.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    content = allData[index].content;
                    showDialog(
                      context: context,
                      builder: (BuildContext bc) {
                        return Result(content, "librery");
                      },
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    alignment: Alignment.centerLeft,
                    height: 60,
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 0.7,
                          color: Colors.grey.shade300,
                        ),
                      ),
                    ),
                    child: Text(
                      allData[index].date,
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
                    ),
                  ),
                );
              },
            ),
    );
  }
}
