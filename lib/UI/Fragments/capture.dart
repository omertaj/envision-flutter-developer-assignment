import 'package:camera/camera.dart';
import 'package:envision_test/Models/content.dart';
import 'package:envision_test/UI/result.dart';
import 'package:envision_test/Utils/apiCalls.dart';
import 'package:flutter/material.dart';
import '../../Utils/Theme.dart';

class Capture extends StatefulWidget {
  CameraController cameraController;
  Capture(this.cameraController);
  @override
  _CaptureState createState() => _CaptureState();
}

class _CaptureState extends State<Capture> {
  Content content;

  bool isLoading = false;

  Widget _cameraPreviewWidget() {
    if (widget.cameraController == null ||
        !widget.cameraController.value.isInitialized) {
      return const Text(
        'Loading Camera',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w900,
        ),
      );
    }
    return CameraPreview(widget.cameraController);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15),
      child: Stack(
        alignment: Alignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
            child: _cameraPreviewWidget(),
          ),
          isLoading
              ? InkWell(
                  onTap: () {
                    setState(() {
                      isLoading = false;
                    });
                  },
                  child: Material(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 100,
                      height: 50,
                      color: AppColors.primary,
                      child: Center(
                        child: Text(
                          "OCR in progress",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                )
              : Positioned(
                  bottom: 30,
                  child: InkWell(
                    onTap: () async {
                      setState(() {
                        isLoading = true;
                      });

                      await widget.cameraController.takePicture().then((img) {
                        ApiCalls().sendImage(img).then((response) {
                          setState(() {
                            isLoading = false;
                          });
                          if (response.statusCode == 200) {
                            if (response.data["message"] == "No text found") {
                              final _snackBar = SnackBar(
                                content: Text(
                                  response.data["message"],
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14),
                                ),
                                duration: const Duration(seconds: 5),
                                backgroundColor: Colors.orange,
                                padding: EdgeInsets.all(10.0),
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(_snackBar);
                            } else {
                              content = Content.fromJson(response.data);
                              showDialog(
                                context: context,
                                builder: (BuildContext bc) {
                                  return Result(content, "capture");
                                },
                              );
                            }
                          } else {
                            final _snackBar = SnackBar(
                              content: Text(
                                "Something went wrong!",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14),
                              ),
                              duration: const Duration(seconds: 5),
                              backgroundColor: Colors.red,
                              padding: EdgeInsets.all(10.0),
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(_snackBar);
                          }
                          print(response);
                        });
                      });
                    },
                    child: Material(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                            color: AppColors.primary),
                        child: Center(
                          child: Text(
                            "CAPTURE",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 25),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
