import 'package:envision_test/Models/cached_data.dart';
import 'package:envision_test/Models/content.dart';
import 'package:envision_test/Utils/Theme.dart';
import 'package:envision_test/Utils/global.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Result extends StatefulWidget {
  Content content;
  String from;
  Result(this.content, this.from);

  @override
  _ResultState createState() => _ResultState();
}

class _ResultState extends State<Result> {
  final f = new DateFormat('dd/MM/yy hh:mm');

  SharedPreferences prefs;

  getCache() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.getString("content") == null ||
        prefs.getString("content").isEmpty) {
      setState(() {
        allData = [];
      });
    } else {
      setState(() {
        allData = CachedData.decode(prefs.getString("content"));
      });
    }
  }

  @override
  void initState() {
    getCache();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 30),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: widget.content.response.paragraphs.length,
                  itemBuilder: (context, index) => Text(
                    widget.content.response.paragraphs[index].paragraph,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontSize: 19),
                  ),
                ),
              ),
              widget.from == "capture"
                  ? InkWell(
                      onTap: () async {
                        final _snackBar = SnackBar(
                          content: Text(
                            'Text saved to librery',
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w400,
                                fontSize: 14),
                          ),
                          duration: const Duration(seconds: 10),
                          backgroundColor: Colors.white,
                          padding: EdgeInsets.all(10.0),
                          action: SnackBarAction(
                            label: 'GO TO LIBRERY',
                            onPressed: () {
                              print('Action is clicked');
                              Navigator.pop(context);
                              tabController.animateTo(1);
                            },
                            textColor: AppColors.primary,
                            disabledTextColor: Colors.grey,
                          ),
                        );

                        CachedData cachedData = new CachedData();
                        cachedData.content = widget.content;
                        cachedData.date = f.format(DateTime.now());
                        allData.add(cachedData);
                        prefs.setString("content", CachedData.encode(allData));

                        ScaffoldMessenger.of(context).showSnackBar(_snackBar);
                      },
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: 60,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50)),
                              color: AppColors.primary),
                          child: Center(
                            child: Text(
                              "SAVE TEXT TO LIBRARY",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    )
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
