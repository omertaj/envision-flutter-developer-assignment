import 'package:flutter/material.dart';

class NoData extends StatefulWidget {
  final String title;
  final String desc;
  const NoData(this.title, this.desc, {Key key}) : super(key: key);

  @override
  _NoDataState createState() => _NoDataState();
}

class _NoDataState extends State<NoData> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(child: Container()),
          CircleAvatar(
            backgroundColor: Colors.deepPurple.shade50,
            radius: 100,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Image.asset(
                "assets/images/empty.png",
                width: 100,
                height: 100,
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            widget.title,
            style: TextStyle(fontSize: 22, color: Colors.black),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            widget.desc,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 14,
                color: Colors.grey.shade400,
                fontWeight: FontWeight.w100),
          ),
          Expanded(flex: 2, child: Container()),
        ],
      ),
    );
  }
}
