class Content {
  Response response;

  Content({this.response});

  Content.fromJson(Map<String, dynamic> json) {
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
  }

  static Map<String, dynamic> toMap(Content content) => {
        'response': content.response,
      };

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['response'] = this.response.toJson();
    }
    return data;
  }
}

class Response {
  List<Paragraphs> paragraphs;

  Response({this.paragraphs});

  Response.fromJson(Map<String, dynamic> json) {
    if (json['paragraphs'] != null) {
      paragraphs = new List<Paragraphs>();
      json['paragraphs'].forEach((v) {
        paragraphs.add(new Paragraphs.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.paragraphs != null) {
      data['paragraphs'] = this.paragraphs.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Paragraphs {
  String paragraph;
  String language;

  Paragraphs({this.paragraph, this.language});

  Paragraphs.fromJson(Map<String, dynamic> json) {
    paragraph = json['paragraph'];
    language = json['language'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['paragraph'] = this.paragraph;
    data['language'] = this.language;
    return data;
  }
}
