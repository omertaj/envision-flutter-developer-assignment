import 'dart:convert';
import 'content.dart';

class CachedData {
  Content content;
  String date;

  CachedData({this.content, this.date});

  CachedData copyWith({CachedData content, DateTime date}) {
    return new CachedData(
        content: content ?? this.content, date: date ?? this.date);
  }

  factory CachedData.fromJson(Map<String, dynamic> jsonData) {
    return CachedData(
      content: Content.fromJson(jsonData['content']),
      date: jsonData['date'],
    );
  }

  static Map<String, dynamic> toMap(CachedData content) => {
        'content': Content.toMap(content.content),
        'date': content.date,
      };

  static String encode(List<CachedData> musics) => json.encode(
        musics
            .map<Map<String, dynamic>>((content) => CachedData.toMap(content))
            .toList(),
      );

  static List<CachedData> decode(String musics) =>
      (json.decode(musics) as List<dynamic>)
          .map<CachedData>((item) => CachedData.fromJson(item))
          .toList();
}
