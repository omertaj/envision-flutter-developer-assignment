import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:envision_test/Models/cached_data.dart';
import 'package:envision_test/Models/content.dart';
import 'package:envision_test/UI/Fragments/capture.dart';
import 'package:envision_test/UI/Fragments/librery.dart';
import 'package:envision_test/Utils/Theme.dart';
import 'package:envision_test/Utils/apiCalls.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'UI/home.dart';
import 'Utils/Theme.dart';
import 'Utils/global.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.deepPurple, fontFamily: "Roboto"),
      home: MyHomePage(),
    );
  }
}
