import 'package:camera/camera.dart';
import 'package:dio/dio.dart';

class ApiCalls {
  static String baseURL = 'https://letsenvision.app/api/test/readDocument';

  Future sendImage(XFile img) async {
    var formData = FormData.fromMap({
      'photo':
          await MultipartFile.fromFile(img.path, filename: img.name.toString()),
    });
    var response = await Dio().post(baseURL, data: formData);

    return response;
  }
}
